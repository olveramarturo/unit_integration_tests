# -*- coding: utf-8 -*-
"""
XML to Tabular tool

Created on November 4, 2019
@author: Arturo Olvera

Summary:
    Turns an xml structured file in to a tabular data structure. It can be 
    imported or executed from the command line

Usage:
    Executed from CLI --> Use the -h option
    Imported -->  xmlParserTool('-d','-t',inFile='path_to_file', outFile='path_to_file')
    Minimal argument is the infile, output file optional, but recomended
"""

import argparse, os
import xml.etree.ElementTree as ET
import xml.etree.ElementPath as EP


class xmlParserTool(object):
    """Parser tool class
    Parameters:
        d describe the XML file
        t list the tags in the xml file
        inFile='path_to_file' pass the location of the input file
        outFile Path to output result to file
        
    Returns:
        If no options selected --> parse full structure
        If -d selected --> output description as string
        If -t selected --> output list object with tags found
        For all options, default behavior is to produce the datastructure 
        object
    """
    
    def __init__(self, *inOptions, **inkwvalues):
        """ Init method, verify arguments passed """
        #print('inOptions ', inOptions)
        if 'd' in inOptions:
            self.optionFlagD = 'd'
        else:
            self.optionFlagD = ''
        if 't' in inOptions:
            self.optionFlagT = 't'
            self.xml_tags()
        else:
            self.optionFlagT = ''
        self.sourceXMLfile = inkwvalues.get('inFile')
        if os.path.isfile(self.sourceXMLfile) == False:
            raise Exception('File not found: ', self.sourceXMLfile)
        self.exitTabularfile = inkwvalues.get('outFile')
        # create the xml parse object to be used in the additional functions
        self.xmlObject = ET.parse(self.sourceXMLfile)
        self.xmlroot = self.xmlObject.getroot()
        self.xmlLevels = self.xml_depth(0)
        #print(self.xmlLevels)

    def output(self):
        """Assemble results when called from CLI"""
        print('Tag List: \n', self.xml_tags())
        print('Max Tag Dept: \n', self.xml_depth())
        
    def xml_structure(self):
        """Optional Method parse headers/structure to describe xml file"""
        print('Description of XML structure')
        
    def xml_tags(self):
        """Optional method, crawl trough xml structure and produce taglists"""
        _tagList = []
        for item in self.xmlObject.iter():
            _tagName = str(item.tag).split('}')
            if _tagName[1] not in _tagList:
                _tagList.append(_tagName[1])
        
        return _tagList
    
    def xml_parse(self):
        """Default method, parse tags/values to object"""
        print('Primary result, tags and values')
    
    def xml_depth(self, to_stop=None):
        """
        Get the max dept of elements in the XML structure
        """
        a_stack = list()
        _getLevels = list()
        _tagLevel = list()
        #Start from the root element in the XML
        a_stack.append(iter([self.xmlroot]))
        while a_stack:
            _currentTag = next(a_stack[-1], None)
            if _currentTag == None:
                a_stack.pop()
            else:
                a_stack.append(iter(_currentTag))
                if to_stop == None or _currentTag.tag == to_stop:
                    _getLevels.append(len(a_stack) - 1)

        return _getLevels

    def xml_maxDepth(self):
        """ Get the max depth from xml_depth()"""
        levelTop = 0
        levels = self.xml_depth()
        for level in levels:
            if int(level) > levelTop:
                levelTop = int(level)
        
        return levelTop
    
def main():
    """Main() execution from CLI """
    #Instanciate argparse object as ap
    ap = argparse.ArgumentParser(prog='xmlParserTool')
    ap.add_argument("-d", 
                    action='store_true', 
                    help="Provide a description of the XML structure")
    ap.add_argument("-t", 
                    action='store_true',
                    help="Provide a list of tags listed in the XML")
    reqarg = ap.add_argument_group("required")
    reqarg.add_argument("-i", "--input", 
                    required=True, 
                    help="Path to input XML file")
    reqarg.add_argument("-o", "--output", 
                    required=True,
                    help="Path to output file")
    args = ap.parse_args()
    #print(args)
    if args.d == True:
        optionFlagD = 'd'
    else:
        optionFlagD = ''
    if args.t ==True:
        optionFlagT = 't'
    else:
        optionFlagT = ''
    sourceXMLfile = args.input
    exitTabularfile = args.output
    #Assemble the call to the main tool
    xmlValues = xmlParserTool(optionFlagD, 
                              optionFlagT, 
                              inFile=sourceXMLfile, 
                              outFile=exitTabularfile)
    return xmlValues

if __name__ == '__main__':
    """ """
    main()