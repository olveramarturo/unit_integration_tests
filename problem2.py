# -*- coding: utf-8 -*-
"""[Module name]

Created on Wed Dec  4 14:19:40 2019
@author: olver

Summary:
  
    
Parameters:

Returns:

Raises:

Comments:

"""

# Least significant bit
def par_non(num):
    if num & 1 == 0:
        print(num, ' es numero par\n')
    else:
        print(num, ' es numero impar\n')
        
print('test1 (4)=========================')
par_non(4)
print('test2 (123456789)=================')
par_non(123456789)
print('test3 (75846)=====================')
par_non(75846)
print('test4 (45783)=====================')
par_non(45783)