# -*- coding: utf-8 -*-
"""xml2tabular.py

Created on November 4, 2019
@author: Arturo Olvera

Summary:
    Turns an xml structured file in to a tabular data structure, built from 
    the specified list of element arguments (default is all elements)

Parameters:
    *kwargs : dict
        input two keyword arguments: xmlfile and elements list both strings

Returns:
    tablist: list
        data structure composed of a list of dictionaries that represent the XML
        structure.
"""
import xml.etree.ElementTree as ET
import os

class XmlImport:
    """Usage: x = XmlImport("file="path-to-file", tags="tag1, tag2, tag3")"""
    
    def __init__(self, **kwargs):
        """
        Upon instanciation, verify input: check for valid file and pass a list
        of tag values.
        """
        # verify the arguments contain real data, assign to local variables
        if len(kwargs.items()) < 2:    #Should present 2 arguments
            raise Exception('Arguments file, tags missing')
        elif len(kwargs.items()) == 2:    # we have 2 arguments
            args = kwargs.items()
            try:
                filedir = args["file"]
                if os.path.isfile(filedir) == True:
                    self.xmlfile = filedir
                else:
                    raise Exeption('File not found: {0}', filedir)
    
    def main(self):
        """Primary execution of the parsing routine"""
        pass

    def parseXML(self, xmlfile):
        # create element tree object\n",
        tree = ET.parse(xmlfile)
    
        # get root element\n",
        root = tree.getroot()
        print(root.text)

        for item in root.iter():
            clean_tag = normalize(item.tag)
            # if the tag matches any of the submitted variables
            print(clean_tag)
        

    def normalize(self, strvalue):
        if strvalue[0] == '{':
            uri, tag = strvalue[1:].split('}')
            return tag
        else:
            return strvalue
      
      
      
"""      
        for child in item:
            if child.tag == '{http://www.opengis.net/kml/2.2}Placemark':
                for x in child:
                    if x.tag == '{http://www.opengis.net/kml/2.2}name':
                        pname = x.text.strip()
                    elif x.tag == '{http://www.opengis.net/kml/2.2}Point':
                        for p in x.iter():
                            if p.tag == '{http://www.opengis.net/kml/2.2}coordinates':
                                ploc = p.text.strip()
                                route_stop = {'name':pname,'location':ploc}
                                route_stops.append(route_stop)
#    return route_stops
    
def main():

    # parse xml call
    route_list = parseXML('Universitaria.kml')
    
    print(route_list)
"""
    
if __name__ == "__main__":

    # Call main function
    main()