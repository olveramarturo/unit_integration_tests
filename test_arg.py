# -*- coding: utf-8 -*-
"""[Module name]

Created on Tue Nov 12 17:00:57 2019
@author: olver

Summary:
  
    
Parameters:

Returns:

Raises:

Comments:

"""

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("echo", help="echo the string you use here")
parser.add_argument("--verbosity", help="Increase output verbosity", 
                    action="store_true")
args = parser.parse_args()
if args.verbose:
    print('verbosity turned on')