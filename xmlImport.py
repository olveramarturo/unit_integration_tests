# -*- coding: utf-8 -*-
"""XML to Tabular tool

Created on November 4, 2019
@author: Arturo Olvera

Summary:
    Turns an xml structured file in to a tabular data structure, built from 
    the specified list of element arguments (default is all elements)

Parameters:
    *kwargs : dict
        input two keyword arguments: xmlfile and elements list both strings

Returns:
    tablist: list
        data structure composed of a list of dictionaries that represent the XML
        structure.
"""
import xml.etree.ElementTree as ET
import os, toolbox

_debug = 0

class NoSourceError(Exception): pass

class XmlImport:
    """import data from the specified XML file"""
    
    def __init__(self, grammar, source=None):
        """Start data upload??"""
        self.loadSource(source and source or self.getDefaultSource())
        self.refresh()
        
    def _load(self, source):
        """Load XML imput source, return parsed XML document"""
        
    
    def main(self):
        """Primary execution of the parsing routine"""
        pass

    def parseXML(self, xmlfile):
        # create element tree object\n",
        self.tree = ET.parse(xmlfile)
    
        # get root element\n",
        root = self.tree.getroot()
        print(root.text)

        for item in root.iter():
            clean_tag = self.normalize(item.tag)
            # if the tag matches any of the submitted variables
            print(clean_tag)
    
    
    def normalize(self, strvalue):
        """Remove the namespace reference from the element tags"""
        if strvalue[0] == '{':
            uri, tag = strvalue[1:].split('}')
            return tag
        else:
            return strvalue
    
if __name__ == "__main__":
    """execution from file"""
    print('Start import...')