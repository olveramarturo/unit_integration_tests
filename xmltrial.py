# -*- coding: utf-8 -*-
"""[Module name]

Created on Wed Jan 22 14:10:20 2020
@author: olver

Summary:
  
    
Parameters:

Returns:

Raises:

Comments:

"""

import xml.etree.ElementTree as ET
import pandas as PD


def _TagAndLevel():
    """Get tag name and dept level."""
    xmlObject = ET.parse('universitaria.kml')
    xmlroot = xmlObject.getroot()
    return xml_depth(xmlroot)


def _stripNS(tagtxt):
    """."""
    _tagName = str(tagtxt).split('}')
    return _tagName[1]


def xml_depth(xmlroot, to_stop=None):
    """Get the max dept of elements in the XML structure."""
    a_stack = list()
    _getLevels = list()
    _tagLevel = list()
    _idxTag = 0
    # Start from the root element in the XML
    a_stack.append(iter([xmlroot]))
    while a_stack:
        _currentTag = next(a_stack[-1], None)
        if _currentTag is None:
            a_stack.pop()
        else:
            a_stack.append(iter(_currentTag))
            if to_stop is None or _currentTag.tag == to_stop:
                _getLevels.append(len(a_stack) - 1)
                cleanTag = _stripNS(_currentTag.tag)
                cleanText = _currentTag.text
                # print(cleanTag, "==", str(len(a_stack) - 1))
                _tagLevel.append((_idxTag,
                                  str(len(a_stack) - 1),
                                  cleanTag,
                                  cleanText))
                _idxTag = _idxTag + 1
    return _tagLevel


if __name__ == "__main__":

    # print(_TagAndLevel())
    df = PD.DataFrame(_TagAndLevel(), columns=['idx', 'level', 'tag', 'value'])
    newdf = df.groupby(['level', 'tag'])
    print(PD.pivot_table(df, index=['tag', 'value']))
    print(newdf)

